Source: pdfgrep
Section: utils
Priority: optional
Maintainer: Christoph Egger <christoph@debian.org>
Build-Depends: asciidoc,
               automake,
               debhelper (>= 10~),
               docbook-xml,
               libgcrypt20-dev,
               libpcre2-dev,
               libpoppler-cpp-dev,
               pkgconf
Standards-Version: 4.1.4
Homepage: https://pdfgrep.org
Vcs-Git: https://salsa.debian.org/debian/pdfgrep.git
Vcs-Browser: https://salsa.debian.org/debian/pdfgrep

Package: pdfgrep
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: search in pdf files for strings matching a regular expression
 Pdfgrep is a tool to search text in PDF files. It works similar to
 `grep'.
 .
 Features:
  - search for regular expressions.
  - support for some important grep options, including:
    + filename output.
    + page number output.
    + optional case insensitivity.
    + count occurrences.
  - and the most important feature: color output!
