pdfgrep (2.2.0-0.1) UNRELEASED; urgency=medium

  [ Raul Cheleguini ]
  * Non-maintainer upload.
  * New upstream release.
  * d/control: Update Build-Depends to include pcre2, pkgconf and others.
    (Closes: #1000003).

 -- Raul Cheleguini <raul.cheleguini@gmail.com>  Tue, 26 Mar 2024 11:57:16 -0300

pdfgrep (2.1.2-2) UNRELEASED; urgency=medium

  * Bump debhelper from old 9 to 10.
  * Re-export upstream signing key without extra signatures.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Name (from
    ./configure), Repository, Repository-Browse.
  * Drop unnecessary dh arguments: --parallel

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 11 Feb 2021 09:03:22 -0000

pdfgrep (2.1.2-1) unstable; urgency=medium

  * enable parallel build
  * Bump standards version to 4.3.0
  * Update URLs
  * Move to salsa

 -- Christoph Egger <christoph@debian.org>  Mon, 07 Jan 2019 22:14:56 +0100

pdfgrep (2.0.1-1) unstable; urgency=medium

  * New upstream release

 -- Christoph Egger <christoph@debian.org>  Sun, 01 Oct 2017 14:51:45 +0200

pdfgrep (2.0-1) unstable; urgency=medium

  * New upstream release

 -- Christoph Egger <christoph@debian.org>  Wed, 25 Jan 2017 10:52:40 +0100

pdfgrep (1.4.1-2) unstable; urgency=medium

  * enable full hardening

 -- Christoph Egger <christoph@debian.org>  Sat, 05 Dec 2015 15:05:42 +0100

pdfgrep (1.4.1-1) unstable; urgency=medium

  * New upstream release
  * Update homepage field

 -- Christoph Egger <christoph@debian.org>  Fri, 02 Oct 2015 09:19:52 +0200

pdfgrep (1.4.0-2) unstable; urgency=medium

  * add libpcre build-dependency

 -- Christoph Egger <christoph@debian.org>  Fri, 14 Aug 2015 13:31:37 +0200

pdfgrep (1.4.0-1) unstable; urgency=medium

  * New upstream release
  * bump standards version

 -- Christoph Egger <christoph@debian.org>  Fri, 14 Aug 2015 12:42:55 +0200

pdfgrep (1.3.2-2) unstable; urgency=medium

  * upload to unstable

 -- Christoph Egger <christoph@debian.org>  Sun, 03 May 2015 12:46:52 +0200

pdfgrep (1.3.2-1) experimental; urgency=medium

  * New upstream release

 -- Christoph Egger <christoph@debian.org>  Mon, 23 Feb 2015 14:10:18 +0100

pdfgrep (1.3.1-1) unstable; urgency=medium

  * New upstream release
    * INCOMPATIBLE CHANGE: -r doesn't follow symlinks
    * A zsh completion module
    * Support for password-protected PDFs
    * Allow to omit '.' with -r to search current directory
    * Add -p or --page-count to count matches per page (by Jascha Knack)
    * Add -m or --max-count to limit matches per file (by Thibault Marin)

 -- Christoph Egger <christoph@debian.org>  Thu, 14 Aug 2014 13:19:32 +0200

pdfgrep (1.3.0-2) sid; urgency=medium

  * Add upstream signing key
  * set compat to 9 for auto-hardening
  * remove libpoppler-private-dev from build-deps (Closes: #751533)
  * move from private git to collab-maint
  * Bump standards version

 -- Christoph Egger <christoph@debian.org>  Fri, 04 Jul 2014 19:57:44 +0200

pdfgrep (1.3.0-1) unstable; urgency=low

  * New upstream release
  * build-depend on pkg-config (Closes: #661081)
  * build-depend on libpoppler-private-dev instead of libpoppler-dev
    (Closes: #660110)
  * Bump standards version from 3.9.1 to 3.9.3 (no further changes)

 -- Christoph Egger <christoph@debian.org>  Mon, 09 Apr 2012 13:19:54 +0200

pdfgrep (1.2-1) unstable; urgency=low

  * New upstream release
  * Bump standards version 3.9.0 -> 3.9.1, no change

 -- Christoph Egger <christoph@debian.org>  Fri, 18 Mar 2011 00:37:02 +0100

pdfgrep (1.1-1) unstable; urgency=low

  * New upstream release
    * Drop patch again, was taken from upstream VCS and already in this release
  * Policy 3.9.0 (no changes)

 -- Christoph Egger <christoph@debian.org>  Wed, 30 Jun 2010 18:26:03 +0200

pdfgrep (1.0-2) unstable; urgency=medium

  * Fix buffer overrun for some pdfs

 -- Christoph Egger <christoph@debian.org>  Tue, 15 Jun 2010 00:13:11 +0200

pdfgrep (1.0-1) unstable; urgency=low

  * Initial release (Closes: #580065)

 -- Christoph Egger <christoph@debian.org>  Mon, 03 May 2010 10:43:01 +0200
